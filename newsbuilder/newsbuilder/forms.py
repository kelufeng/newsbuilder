from django.db import models
from django.forms import ModelForm
from django import forms
from newslibrary.models import *

class PublicationForm(ModelForm):
	class Meta:
		model = Publication

class JournalistForm(ModelForm):
	class Meta:
		model = Journalist

class ArticleForm(ModelForm):
	class Meta:
		model = Article

class PostForm(ModelForm):
	class Meta:
		model = Post