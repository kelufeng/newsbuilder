from django.template import Context, loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from newslibrary.models import *
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.forms.models import modelformset_factory
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout
from itertools import chain

@login_required
def index(request):
	return render_to_response('newsbuilder/index.html', context_instance=RequestContext(request))
	
#group_name in [string.lower(g.name) for g in u.groups.all()
	
@login_required
def articles(request):
	u = request.user
	article_list = Article.objects.filter(region="Mars")
	for g in u.groups.all():
		query_list = Article.objects.filter(region=g.name)
		article_list = list(chain(article_list, query_list))
	return render_to_response('newsbuilder/article_list.html', {'article_list': article_list}, context_instance=RequestContext(request))

#@login_required
#def articles(request):
#	article_list = Article.objects.all().order_by('-pub_date')
#	return render_to_response('newsbuilder/article_list.html', {'article_list': article_list}, context_instance=RequestContext(request))

@login_required
def article_detail(request, article_id):
	article = get_object_or_404(Article, pk=article_id)
	return render_to_response('newsbuilder/article_detail.html', {'article': article}, context_instance=RequestContext(request))

@login_required
def article_add(request):
	ArticleFormSet = modelformset_factory(Article)
	formset = ArticleFormSet(queryset=Article.objects.none())
	return render_to_response('newsbuilder/article_add.html', {"formset": formset}, context_instance=RequestContext(request))

@login_required
def article_save(request):
	if request.method == 'POST':
		ArticleFormSet = modelformset_factory(Article)
		formset = ArticleFormSet(request.POST, request.FILES)
		if formset.is_valid():
			formset.save()
	return HttpResponseRedirect('/articles')

@login_required
def article_edit(request, article_id):
	article = get_object_or_404(Article, pk=article_id)
	return render_to_response('newsbuilder/article_edit.html', {'article': article}, context_instance=RequestContext(request))



@login_required
def publications(request):
	publication_list = Publication.objects.all().order_by('-modified')
	return render_to_response('newsbuilder/publication_list.html', {'publication_list': publication_list}, context_instance=RequestContext(request))

@login_required
def publication_detail(request, publication_id):
	publication = get_object_or_404(Publication, pk=publication_id)
	return render_to_response('newsbuilder/publication_detail.html', {'publication': publication}, context_instance=RequestContext(request))

@login_required
def publication_add(request):
	PublicationFormSet = modelformset_factory(Publication)
	formset = PublicationFormSet(queryset=Publication.objects.none())
	return render_to_response('newsbuilder/publication_add.html', {"formset": formset}, context_instance=RequestContext(request))

@login_required
def publication_save(request):
	if request.method == 'POST':
		PublicationFormSet = modelformset_factory(Publication)
		formset = PublicationFormSet(request.POST, request.FILES)
		if formset.is_valid():
			formset.save()
	return HttpResponseRedirect('/publications')

@login_required
def publication_edit(request, article_id):
	article = get_object_or_404(Article, pk=article_id)
	return render_to_response('newsbuilder/article_edit.html', {'article': article}, context_instance=RequestContext(request))





@login_required
def journalists(request):
	journalist_list = Journalist.objects.all().order_by('-modified')
	return render_to_response('newsbuilder/journalist_list.html', {'journalist_list': journalist_list}, context_instance=RequestContext(request))

@login_required
def journalist_detail(request, journalist_id):
	journalist = get_object_or_404(Journalist, pk=journalist_id)
	return render_to_response('newsbuilder/journalist_detail.html', {'journalist': journalist}, context_instance=RequestContext(request))

@login_required
def journalist_add(request):
	JournalistFormSet = modelformset_factory(Journalist)
	formset = JournalistFormSet(queryset=Journalist.objects.none())
	return render_to_response('newsbuilder/journalist_add.html', {"formset": formset}, context_instance=RequestContext(request))

@login_required
def journalist_save(request):
	if request.method == 'POST':
		JournalistFormSet = modelformset_factory(Journalist)
		formset = JournalistFormSet(request.POST, request.FILES)
		if formset.is_valid():
			formset.save()
	return HttpResponseRedirect('/journalists')

@login_required
def journalist_edit(request, journalist_id):
	journalist = get_object_or_404(Journalist, pk=article_id)
	return render_to_response('newsbuilder/journalist_edit.html', {'journalist': journalist}, context_instance=RequestContext(request))


@login_required
def preview_newsletter(request):
	articles = Article.objects.all().order_by('region')
	regions = Region.objects.all().order_by('name')
	return render_to_response('preview.html', {'articles': articles, 'regions': regions}, context_instance=RequestContext(request))


@login_required
def posts(request):
	unsent_posts_list = Post.objects.all().order_by('-post_date')
	output = ', '.join([p.content for p in unsent_posts_list])
	return HttpResponse(output)
	
@login_required
def article_archive(request):
	archive_list = Article.objects.all().order_by('-pub_date')
	output = ', '.join([a.headline for a in archive_list])
	return HttpResponse(output)

@login_required
def post_archive(request):
	archive_list = Post.objects.all().order_by('-post_date')
	output = ', '.join([p.content for p in archive_list])
	return HttpResponse(output)

@login_required
def logout_page(request):
	logout(request)
	return HttpResponseRedirect('/')