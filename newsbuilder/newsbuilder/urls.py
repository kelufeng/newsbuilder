from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'newsbuilder.views.index'),
    # url(r'^newsbuilder/', include('newsbuilder.foo.urls')),
	url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
	url(r'^logout/$', 'newsbuilder.views.logout_page'),

	url(r'^articles/$', 'newsbuilder.views.articles'),
	url(r'^articles/(?P<article_id>\d+)/$', 'newsbuilder.views.article_detail'),
	url(r'^articles/add/$', 'newsbuilder.views.article_add'),
	url(r'^articles/save/$', 'newsbuilder.views.article_save'),
	url(r'^articles/edit/(?P<article_id>\d+)/$', 'newsbuilder.views.article_edit'),
	
	url(r'^publications/$', 'newsbuilder.views.publications'),
	url(r'^publications/(?P<publication_id>\d+)/$', 'newsbuilder.views.publication_detail'),
	url(r'^publications/add/$', 'newsbuilder.views.publication_add'),
	url(r'^publications/save/$', 'newsbuilder.views.publication_save'),
	url(r'^publications/edit/(?P<publication_id>\d+)/$', 'newsbuilder.views.publication_edit'),
	
	url(r'^journalists/$', 'newsbuilder.views.journalists'),
	url(r'^journalists/(?P<journalist_id>\d+)/$', 'newsbuilder.views.journalist_detail'),
	url(r'^journalists/add/$', 'newsbuilder.views.journalist_add'),
	url(r'^journalists/save/$', 'newsbuilder.views.journalist_save'),
	url(r'^journalists/edit/(?P<journalist_id>\d+)/$', 'newsbuilder.views.journalist_edit'),
	
	url(r'^posts/$', 'newsbuilder.views.posts'),

	url(r'^preview/$', 'newsbuilder.views.preview_newsletter'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
