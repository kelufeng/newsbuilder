from django.db import models
from django.contrib.auth.models import User

REGIONS = (
    ('Australia', 'Australia'),
    ('China', 'China'),
    ('Hong Kong', 'Hong Kong'),
    ('India', 'India'),
    ('Indonesia', 'Indonesia'),
    ('Malaysia', 'Malaysia'),
    ('Philippines', 'Philippines'),
    ('Singapore', 'Singapore'),
    ('Taiwan', 'Taiwan'),
    ('Thailand', 'Thailand'),
    ('Vietnam', 'Vietnam'),
)

LANGUAGE_CHOICES = (
    ('EN', 'English'),
    ('ZH', 'Chinese'),
    ('ID', 'Indonesian'),
    ('MS', 'Malay'),
    ('JA', 'Japanese'),
    ('JA', 'Japanese'),
    ('TH', 'Thai'),
    ('VI', 'Vietnamese'),
)

CATEGORIES = (
    ('BlackBerry', 'BlackBerry'),
    ('Competitor', 'Competitor'),
    ('Industry', 'Industry'),
)

SUBJECTS = (
    ('BlackBerry 10', 'BlackBerry 10'),
    ('Bold', 'Bold'),
    ('Torch', 'Torch'),
    ('Curve', 'Curve'),
    ('PlayBook', 'PlayBook'),
)

SENTIMENTS = (
    ('+', 'Positive'),
    ('=', 'Neutral'),
    ('-', 'Negative'),
)

MEDIUM = (
    ('Print', 'Print'),
    ('Online', 'Online'),
    ('Radio', 'Radio'),
    ('TV', 'Television'),
)

PUB_TYPE = (
    ('Newspaper', 'Newspaper'),
    ('Tabloid', 'Tabloid'),
    ('Magazine', 'Magazine'),
)

TIER = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
)

FREQUENCY = (
    ('Daily', 'Daily'),
    ('Weekly', 'Weekly'),
    ('Monthly', 'Monthly'),
)

class Region(models.Model):
	name = models.CharField(max_length=250, choices=REGIONS)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name

class Category(models.Model):
	name = models.CharField(max_length=250)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name

class Subject(models.Model):
	name = models.CharField(max_length=250)
	category = models.ForeignKey(Category)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name

class Sentiment(models.Model):
	description = models.CharField(max_length=250)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name
		
class Image(models.Model):
	image = models.ImageField(upload_to='images/%Y/%m/%d')
	description = models.CharField(max_length=250, blank=True)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name

class File(models.Model):
	document = models.FileField(upload_to='documents/%Y/%m/%d')
	description = models.CharField(max_length=250, blank=True)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name

class Publication(models.Model):
	name = models.CharField(max_length=250)
	medium = models.CharField(max_length=250, choices=MEDIUM)
	publication_type = models.CharField(max_length=250, choices=PUB_TYPE)
	url = models.CharField(max_length=250, blank=True)
	current_circulation = models.IntegerField()
	tier = models.CharField(max_length=1, choices=TIER)
	frequency = models.CharField(max_length=250, choices=FREQUENCY)
	region = models.CharField(max_length=250, choices=REGIONS)
	language = models.CharField(max_length=2, choices=LANGUAGE_CHOICES)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name
	
class Journalist(models.Model):
	publication = models.ForeignKey(Publication)
	given_name = models.CharField(max_length=250)
	surname = models.CharField(max_length=250)
	focus = models.CharField(max_length=250, blank=True)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return (self.given_name + ' ' + self.surname)

class Article(models.Model):
	publication = models.ForeignKey(Publication)
	journalist = models.ForeignKey(Journalist, blank=True, null=True)
	pub_date = models.DateField('date published', blank=True)
	region = models.CharField(max_length=250, blank=True, choices=REGIONS)
	headline = models.CharField(max_length=250)
	circulation = models.IntegerField()
	subject = models.CharField(max_length=250, choices=SUBJECTS)
	category = models.CharField(max_length=250, choices=CATEGORIES)
	sentiment = models.CharField(max_length=250, choices=SENTIMENTS)
	language = models.CharField(max_length=250, blank=True, choices=LANGUAGE_CHOICES)
	content = models.TextField()
	translated_content = models.TextField(blank=True)
	link = models.CharField(max_length=250, blank=True)
	clipping = models.ForeignKey(File, blank=True, null=True)
	image = models.ForeignKey(Image, blank=True, null=True)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	last_modified_by = models.ForeignKey(User, blank=True, null=True, related_name="article_modified_set")
	created_by = models.ForeignKey(User, blank=True, null=True, related_name="article_created_set")

class Site(models.Model):
	name = models.CharField(max_length=250)
	url = models.CharField(max_length=250)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name
	
class Influencer(models.Model):
	site = models.ForeignKey(Site)
	handle = models.CharField(max_length=250)
	profile_link = models.CharField(max_length=250)
	name = models.CharField(max_length=250)
	current_followers = models.IntegerField()
	region = models.CharField(max_length=250, choices=REGIONS)
	language = models.CharField(max_length=250)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name
	
class Post(models.Model):
	site = models.ForeignKey(Site)
	influencer = models.ForeignKey(Influencer)
	post_date = models.DateTimeField('date posted')
	link = models.CharField(max_length=250)
	followers = models.IntegerField()
	shares = models.IntegerField()
	subject = models.CharField(max_length=250, choices=SUBJECTS)
	category = models.CharField(max_length=250, choices=CATEGORIES)
	sentiment = models.CharField(max_length=250, choices=SENTIMENTS)
	language = models.CharField(max_length=250, choices=LANGUAGE_CHOICES)
	content = models.TextField()
	translated_content = models.TextField(blank=True)
	clipping = models.ForeignKey(File, blank=True, null=True)
	image = models.ForeignKey(Image, blank=True, null=True)
	modified = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)