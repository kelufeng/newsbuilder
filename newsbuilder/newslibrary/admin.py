from django.contrib import admin
from django.contrib.auth.models import User
from newslibrary.models import *

class PublicationAdmin(admin.ModelAdmin):
	list_display = ('name', 'region', 'medium', 'tier')

class JournalistAdmin(admin.ModelAdmin):
	list_display = ('given_name', 'surname', 'publication')
	
class InfluencerAdmin(admin.ModelAdmin):
	list_display = ('name', 'handle', 'site', 'region')

class ArticleAdmin(admin.ModelAdmin):
	def region(self, instance):
		return instance.publication.region
	list_display = ('headline', 'publication', 'journalist', 'pub_date', 'region')
	
class PostAdmin(admin.ModelAdmin):
	def region(self, instance):
		return instance.influencer.region
	list_display = ('influencer', 'site', 'content', 'post_date', 'region')

admin.site.register(Publication, PublicationAdmin)
admin.site.register(Site)
admin.site.register(Journalist, JournalistAdmin)
admin.site.register(Influencer, InfluencerAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Region)